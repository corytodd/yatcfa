#YATCFA
##*Yet Another Tip Calculator for Android*

##About
This project was created for a software design course. I am not a UI designer and actually spend most of time writing in assember so this was fun.

I chose not to gitignore any files because the assignment must be able to be built by the instructor. I will not leave to chance the possibility of his environment being weird.

##Environment
This was developed on a Windows 7 64-bit machine using ADT, a bundled Android development studio based on Eclipse. After the last Android SDK update, my Android plugins for Eclipse lost their cookies so I chose to go the bundled route. Setup is quick and having dedicated settings for only Android seems to reduce the chance of things breaking.

##Target
Since YATCFA users are all hip and always up to date with the latest Android OS, I've targeted Jellybean and not given too much thought to older clients. Simply put, if you want to run on Froyo, clone the repo and get to work.

Have Fun!

