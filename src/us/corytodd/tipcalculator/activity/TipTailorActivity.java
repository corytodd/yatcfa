package us.corytodd.tipcalculator.activity;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import us.corytodd.tipcalculator.R;
import us.corytodd.tipcalculator.data.Guest;
import us.corytodd.tipcalculator.data.Receipt;
import us.corytodd.utils.Money;
import android.R.color;
import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class TipTailorActivity extends Activity {

	protected static Toast myToast = null;
	private Context context;

	public static final int ROW_MASK = 0x800;;
	public static final int EDIT_TEXT_MASK = 0x400;
	public static final int SLIDER_MASK = 0x200;
	public static final int TEXT_VIEW_MASK = 0x100;
	public static final int CLEAR_MASK = 0xFF;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tip_tailor);
		context = getApplicationContext();

		// Build this for any onSeekBarChanged updates
		// Warning is okay We have a single Toast object to control 
		// "toast stacking"
		myToast = Toast.makeText(context, "", Toast.LENGTH_SHORT);

		redrawTailoredWindow();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			MainActivity.updateLabels();
		}
		return super.onKeyDown(keyCode, event);
	}

	OnClickListener ChangeIsTipTailored = new OnClickListener() {
		public void onClick(View v) {
			Button btn = (Button) v;
			Receipt.INSTANCE.setTipTailored();
			if (Receipt.INSTANCE.isTipTailored()) {
				btn.setText("Tip tailoring is On");
			} else {
				btn.setText("Tip tailoring is Off");
			}
		};
	};

	OnSeekBarChangeListener OnSeekBarProgress = new OnSeekBarChangeListener() {

		public void onProgressChanged(SeekBar s, int progress, boolean touch) {

			if (Receipt.INSTANCE.isTipTailored()) {
				int numGuests = Receipt.INSTANCE.getNumGuests();
				Money tippableBill = Receipt.INSTANCE.getTipableBill();

				// Don't bother if it's just for one!
				if (numGuests > 1) {

					// How much have we deviated from center? This is the
					// percentage
					// for which we adjust the guest's share
					double delta = (progress - 50) / 100.00;

					// The actual tip rate from Receipt. Flat rate for all
					// guests
					double tip = Receipt.INSTANCE.getTipAsDecimal();

					int id = (s.getId() & CLEAR_MASK);

					Guest guest = Receipt.INSTANCE.getGuest(id);

					if (guest != null) {
						guest.setShareOfBill(progress);

						Money adjustment;

						if (progress == 0) {
							// Guest isn't paying tip
							guest.setAmountDue(new Money(BigDecimal.ZERO));
							adjustment = tippableBill.divide(numGuests);
							myToast.setText(guest.getFullName()
									+ getString(R.string.toast_paying_no_tip));
						} else if (progress == 100) {
							// Guest is paying the entire tip
							guest.setAmountDue(Receipt.INSTANCE.getTipTotal());
							adjustment = Receipt.INSTANCE.getTipTotal()
									.multiply(-1);
							myToast.setText(guest.getFullName()
									+ getString(R.string.toast_paying_all_tip));
						} else {
							// Otherwise, Just adjust
							guest.setAmountDue(tippableBill.divide(numGuests)
									.add(tippableBill.multiply(delta)));
							adjustment = Receipt.INSTANCE.getPerPersonTip()
									.subtract(guest.getAmountDue());
							myToast.setText(guest.getFullName()
									+ " " + getString(R.string.toast_set_share_adjustment)
									+ guest.getAmountDue().toString());
						}

						myToast.show();
						Receipt.INSTANCE.makeAdjustment(adjustment);
						Receipt.INSTANCE.calculateTip();
						redrawTailoredWindow();
					}
				}
			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub

		}

	};

	/**
	 * Delete all table rows and redraw. Populate each row wich a Guest object
	 * from the guest list. Dump the list when done!
	 */
	@SuppressWarnings("deprecation")
	private void redrawTailoredWindow() {

		TableLayout tLayout = (TableLayout) findViewById(R.id.guestList);
		tLayout.removeAllViews();

		TableRow tr;
		SeekBar slider;
		TextView guestShare;
		Guest guest;

		// Get the guest list
		HashMap<Integer, Guest> guestList = Receipt.INSTANCE.getGuestList();

		// Iterate and re create the layout
		Iterator<Entry<Integer, Guest>> it = guestList.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Integer, Guest> pairs = it.next();

			guest = (Guest) pairs.getValue();

			tr = new TableRow(context);
			tr.setLayoutParams(new TableRow.LayoutParams(
					TableRow.LayoutParams.WRAP_CONTENT,
					TableRow.LayoutParams.WRAP_CONTENT));
			tr.setId(ROW_MASK | guest.getId());

			EditText txtGuest = new EditText(context);
			txtGuest.setInputType(0x00002001); // Text w/ capital first letters
			txtGuest.setSelectAllOnFocus(true);
			txtGuest.setText(guest.getFullName());
			txtGuest.addTextChangedListener(new GenericTextWatcher(txtGuest));
			txtGuest.setId(EDIT_TEXT_MASK | guest.getId());

			slider = new SeekBar(context);
			slider.setProgress(guest.getShareOfBill());
			slider.setMax(100);
			slider.setOnSeekBarChangeListener(OnSeekBarProgress);
			ShapeDrawable thumb = new ShapeDrawable(new OvalShape());

			thumb.setIntrinsicHeight(80);
			thumb.setIntrinsicWidth(30);
			thumb.setColorFilter(color.holo_blue_bright, PorterDuff.Mode.MULTIPLY);
			slider.setThumb(thumb);
			slider.setId(SLIDER_MASK | guest.getId());

			guestShare = new TextView(context);
			guestShare.setGravity(Gravity.CENTER_VERTICAL);
			guestShare.setText(new DecimalFormat("#.##")
					.format(Receipt.INSTANCE.getTipableBill()
							.multiply(guest.getShareOfBill() / 100.00)
							.toDouble()));
			guestShare.setText(Receipt.INSTANCE.getPerPersonTip() + "");
			guestShare.setId(TEXT_VIEW_MASK | guest.getId());

			/**
			 * Add the editText and slider to the row
			 */
			tr.addView(txtGuest);
			tr.addView(slider);
			tr.addView(guestShare);

			/**
			 * Add the row to the table
			 */
			tLayout.addView(tr, tLayout.getChildCount());
			txtGuest.requestFocus();

		}

		tr = new TableRow(context);
		Button btn = new Button(context);
		btn.setGravity(Gravity.CENTER_VERTICAL);
		btn.setOnClickListener(ChangeIsTipTailored);
		if (Receipt.INSTANCE.isTipTailored()) {
			btn.setText("Tailoring is On");
		} else {
			btn.setText("Tailoring is Off");
		}
		tr.addView(btn);
		tLayout.addView(tr);
	}
}
