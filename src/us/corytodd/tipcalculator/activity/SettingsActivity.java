package us.corytodd.tipcalculator.activity;

import java.text.ParseException;

import us.corytodd.tipcalculator.R;
import us.corytodd.tipcalculator.data.Receipt;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Profile;
import android.widget.Toast;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {
	/**
	 * Determines whether to always show the simplified settings UI, where
	 * settings are presented in a single list. When false, settings are shown
	 * as a master/detail two-pane view on tablets. When true, a single pane is
	 * shown on tablets.
	 */
	private static final boolean ALWAYS_SIMPLE_PREFS = false;
	private static Preference defaultTip;
	private static Preference minTip;
	private static Preference maxTip;
	private static Context context;
	
	@SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 
        addPreferencesFromResource(R.xml.settings);
 
    }
	

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		setupSimplePreferencesScreen();
	}

	/**
	 * Shows the simplified settings UI if the device configuration if the
	 * device configuration dictates that a simplified, single-pane UI should be
	 * shown.
	 */
	@SuppressWarnings("deprecation")
	private void setupSimplePreferencesScreen() {
		if (!isSimplePreferences(this)) {
			return;
		}
		
		context = getApplicationContext();
		
		// Try to figure out the owner's name
		EditTextPreference ep = (EditTextPreference) findPreference("prefUsername");
		String[] SELF_PROJECTION = new String[] { Phone._ID, Phone.DISPLAY_NAME, };
		Cursor cursor = this.getContentResolver().query( Profile.CONTENT_URI, SELF_PROJECTION, null, null, null);
		cursor.moveToFirst();
		String owner = (cursor.getString(1).length() > 0) ? cursor.getString(1) : "Me";
		
		// Don't update the field if the user has already set their name
		if(ep.getText() == null || (!ep.getText().equalsIgnoreCase("me")) && !(ep.getText().length() > 2)) {
			ep.setText(owner);
		}
		
		// Try not to overwrite the currency value if it's set
		ep = (EditTextPreference) findPreference("prefCurrency");
		if(ep.getText() == null || ep.getText().equalsIgnoreCase("") || (ep.getText().length() > 0)) {
			ep.setText(Receipt.INSTANCE.getSymbol());
		}

		
		// Bind the summaries of EditText/List/Dialog/Ringtone preferences to
		// their values. When their values change, their summaries are updated
		// to reflect the new value, per the Android Design guidelines.
		defaultTip = findPreference("prefDefaultTip");
		minTip = findPreference("prefTipMin");
		maxTip = findPreference("prefTipMax");
		
		bindPreferenceSummaryToValue(defaultTip);
		bindPreferenceSummaryToValue(minTip);
		bindPreferenceSummaryToValue(maxTip);
		bindPreferenceSummaryToValue(findPreference("prefUsername"));
		bindPreferenceSummaryToValue(findPreference("prefCurrency"));

	}

	/** {@inheritDoc} */
	@Override
	public boolean onIsMultiPane() {
		return isXLargeTablet(this) && !isSimplePreferences(this);
	}

	/**
	 * Helper method to determine if the device has an extra-large screen. For
	 * example, 10" tablets are extra-large.
	 */
	private static boolean isXLargeTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
	}

	/**
	 * Determines whether the simplified settings UI should be shown. This is
	 * true if this is forced via {@link #ALWAYS_SIMPLE_PREFS}, or the device
	 * doesn't have newer APIs like {@link PreferenceFragment}, or the device
	 * doesn't have an extra-large screen. In these cases, a single-pane
	 * "simplified" settings UI should be shown.
	 */
	private static boolean isSimplePreferences(Context context) {
		return ALWAYS_SIMPLE_PREFS
				|| Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
				|| !isXLargeTablet(context);
	}

	/**
	 * A preference value change listener that updates the preference's summary
	 * to reflect its new value.
	 */
	private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object value) {
			String stringValue = value.toString();

			if (preference instanceof ListPreference) {
				/**
				 *  For list preferences, look up the correct display value in
				 * the preference's 'entries' list.
				 * */
				ListPreference listPreference = (ListPreference) preference;
				int index = listPreference.findIndexOfValue(stringValue);

				/**
				 * Set the summary to reflect the new value.
				 */
				preference
						.setSummary(index >= 0 ? listPreference.getEntries()[index]
								: null);
			} else if (preference instanceof SwitchPreference) {
				/**
				 * Cast that boolean to a non-geek friendly string
				 */
			}
			else {
				/**
				 * For all other preferences, set the summary to the value's
				 * simple string representation.
				 * */
				String prefName = preference.getKey();

				try {
					float newVal = Float.parseFloat(stringValue);
					
					if(prefName.equalsIgnoreCase("prefTipMin")) {	
						EditTextPreference max = (EditTextPreference) maxTip;
						EditTextPreference def = (EditTextPreference) defaultTip;
						if(newVal > Float.parseFloat(max.getText()) || newVal > Float.parseFloat(def.getText()) || newVal > 100) {
							Toast.makeText(context, R.string.bad_min_tip, Toast.LENGTH_LONG).show();
							stringValue = context.getString(R.string.lbl_minimim_tip);
							return false;
						}	
					} else if(prefName.equalsIgnoreCase("prefTipMax")) {
						EditTextPreference min = (EditTextPreference) minTip;
						EditTextPreference def = (EditTextPreference) defaultTip;
						if(newVal < Float.parseFloat(def.getText()) || newVal < Float.parseFloat(min.getText()) || newVal > 100) {
							Toast.makeText(context, R.string.bad_max_tip, Toast.LENGTH_LONG).show();
							stringValue = context.getString(R.string.basic_forty);
							return false;
						}		
					} else if(prefName.equalsIgnoreCase("prefDefaultTip")) {
						EditTextPreference min = (EditTextPreference) minTip;
						EditTextPreference max = (EditTextPreference) maxTip;
						if(newVal > Float.parseFloat(max.getText()) || newVal < Float.parseFloat(min.getText()) || newVal > 100) {
							Toast.makeText(context, R.string.bad_default_tip, Toast.LENGTH_LONG).show();
							stringValue = context.getString(R.string.basic_twenty);
							return false;
						}						
					}
				} catch(NumberFormatException nfe) {
					// Any legit strings will just fall through. Don't worry about validating them.
				}
												
				preference.setSummary(stringValue);		
			}
			
			return true;
		}
	};

	/**
	 * Binds a preference's summary to its value. More specifically, when the
	 * preference's value is changed, its summary (line of text below the
	 * preference title) is updated to reflect the value. The summary is also
	 * immediately updated upon calling this method. The exact display format is
	 * dependent on the type of preference.
	 * 
	 * @see #sBindPreferenceSummaryToValueListener
	 */
	private static void bindPreferenceSummaryToValue(Preference preference) {
		// Set the listener to watch for value changes.
		preference
				.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

		// Trigger the listener immediately with the preference's
		// current value.
		sBindPreferenceSummaryToValueListener.onPreferenceChange(
				preference,
				PreferenceManager.getDefaultSharedPreferences(
						preference.getContext()).getString(preference.getKey(),
						""));
	}
	
	/**
	 * Override so that we can update the main UI with new preferences without having to start the app over
	 */
	@Override
	public void onDestroy() {
	    super.onDestroy();
		try {
			Receipt.INSTANCE.adjustConfigurables();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
