package us.corytodd.tipcalculator.activity;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class BaseActivity extends Activity {
	/**
	 * Return the R.id name of a view
	 * 
	 * @param v
	 *            View for which a the string id is desired
	 * @return string R.id value
	 */
	private static String viewIdToString(View v) {
		int id = v.getId();
		String result = "na";
		if (id != View.NO_ID) {
			Resources res = v.getResources();
			if (res != null) {
				result = res.getResourceEntryName(id);
			}
		}
		return result;
	}

	/**
	 * For a given TableLayour, find all widgets of type widget type with an id that starts with idString
	 * and set the text to newText
	 * @param activity The activity target
	 * @param tLayout	The TablLayout containing rows that will be updated
	 * @param widgetType The target widget type contained within the TableRow
	 * @param idSubstring R.id string the prefixes your target widget
	 * @param newText The string the setText will apply to the widget
	 */
	protected static void updateTableRowWidgets(Activity activity, TableLayout tLayout, Class widgetType, String idSubstring, String newText) {

		View v;
		String id;
		TextView tv;
		TableRow tr;

		for (int i = 0; i < tLayout.getChildCount(); i++) {

			v = tLayout.getChildAt(i);
			id = viewIdToString(v);

			// Check if we have a table row
			if (v.getClass() == TableRow.class) {
				tr = (TableRow) activity.findViewById(v.getId());

				for (int j = 0; j < tr.getChildCount(); j++) {
					
					v = tr.getChildAt(j);
					id = viewIdToString(v);				

					// Do we have a the target widget type with a matching, valid id? 
					if (!id.equalsIgnoreCase("na")
							&& id.startsWith(idSubstring)
							&& v.getClass() == TextView.class) {
						tv = (TextView) activity.findViewById(v.getId());
						tv.setText(newText);
					}
				}
			}

		}
	}
}
