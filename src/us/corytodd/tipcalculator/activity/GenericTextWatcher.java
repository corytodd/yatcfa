package us.corytodd.tipcalculator.activity;

import us.corytodd.tipcalculator.data.Guest;
import us.corytodd.tipcalculator.data.Receipt;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

public class GenericTextWatcher implements TextWatcher{

    private View v;
    GenericTextWatcher(View view) {
        this.v = view;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){
    	
    }
    
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
	}

    public void afterTextChanged(Editable editable) {
		int index = v.getId() & TipTailorActivity.CLEAR_MASK;
		Guest guest = Receipt.INSTANCE.getGuest(index);
		String newText = ((EditText)v).getText().toString();
		if( newText != null && guest != null)
			guest.setFullName(newText);	
    }
}

