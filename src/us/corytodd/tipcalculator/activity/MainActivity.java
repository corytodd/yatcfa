package us.corytodd.tipcalculator.activity;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

import us.corytodd.tipcalculator.R;
import us.corytodd.tipcalculator.data.Guest;
import us.corytodd.tipcalculator.data.Receipt;
import us.corytodd.utils.Money;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Tip Calculator App for CSE598
 * All code by Cory Todd
 * 
 * All images and Icons sourced from Adam Fingerman. http://arctouch.com/2010/01/tip-em-tip-calculator-for-blackberry-curve-bold-pearl/
 */

public class MainActivity extends BaseActivity {

	private static SharedPreferences preferences;
	private static Context context;
	private static Activity activity;
	
	private static TextView tipRate;
	private static TextView totalTip;
	private static TextView perPersonTip;
	private static TextView total;
	private static TableLayout layout;
	protected static Toast myToast = null;


	private final int MAX_GUESTS = 99;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// All prefences are stored using the Android Preference Manager.
		RatingBar quality = (RatingBar) findViewById(R.id.ratingBar);
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		context = getApplicationContext();
		activity = this;
		myToast = Toast.makeText(context, "", Toast.LENGTH_SHORT);			
		
		tipRate = (TextView) findViewById(R.id.lbl_tipRate_actualValue);
		totalTip = (TextView) findViewById(R.id.lbl_totalTip_actualValue);
		perPersonTip = (TextView) findViewById(R.id.lbl_perPersonTip_actualValue);
		total = (TextView) findViewById(R.id.lbl_totalBill_actualValue);
		layout = (TableLayout) findViewById(R.id.table);
		populateSpinner();

		/**
		 * Receipt object is an enum singleton (Bloch, Effective Java, Item 3)
		 */
		try {
			Money.init(getCurrencyByLocation(this), RoundingMode.HALF_EVEN);
			Receipt.INSTANCE.initialize(context, quality.getNumStars());
			Receipt.INSTANCE.addGuest(new Guest(preferences.getString("prefUsername", "Just me!")), 1);
		} catch (ParseException e) {
			e.printStackTrace();
			myToast.setText(R.string.bad_application_init);
			myToast.show();
			finish();
		}

		setListeners();

		/**
		 * Finally set the UI labels with the default Receipt instance's values
		 */
		MainActivity.updateLabels();

	}

	/**
	 * All user-editable UI elements will have a listener to 1) validate data 2)
	 * update associated object 3) trigger a tip calculation if possible
	 */
	private void setListeners() {

		/**
		 * Listen for changes to the number of guests listener
		 */
		Spinner numGuests = (Spinner) findViewById(R.id.spinner_NumGuests);
		numGuests
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,
							View view, int newSize, long id) {
												
						Receipt.INSTANCE.setNumGuests(Integer.parseInt(parent
								.getItemAtPosition(newSize).toString()));
						
						int oldSize = Receipt.INSTANCE.getGuestListSize();
						
						
						/***
						 * ADD 1 to the newSize becuase the listener is zero based!!! I wasted hours on this!!!
						 */
						newSize++;
						
						// Did we add guests?
						if(oldSize < newSize) {
							//Start creating new guests
							for(int i=2; i<(newSize - oldSize) + 2; i++) {
								Receipt.INSTANCE.addGuest(new Guest("Guest_"), i);
							}
						} else if(oldSize > newSize) {
							// Start removing guests from the end
							for(int i=oldSize; i >newSize; i--) {
								Receipt.INSTANCE.removeLastGuest();
							}
						} else {
							// Otherwise nothing changed which isn't really possible. Ignore.
						}
						
												
						Receipt.INSTANCE.calculateTip();	
						updateLabels();
					}

					public void onNothingSelected(AdapterView<?> arg0) {
					}
				});

		/**
		 * Listen for changes to the rating bar
		 */
		RatingBar quality = (RatingBar) findViewById(R.id.ratingBar);
		quality.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			public void onRatingChanged(RatingBar ratingBare, float rating,
					boolean fromUser) {
				Receipt.INSTANCE.setRating((int) Math.round(rating));
				Receipt.INSTANCE.calculateTip();
				updateLabels();
			}
		});

		/**
		 * Listen for change to the bill total field. Validate!
		 */
		EditText billTotal = (EditText) findViewById(R.id.txt_billTotal);
		billTotal.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				try {
					Money billItemsOnly = new Money(s.toString());
					Money tax = Receipt.INSTANCE.getTaxTotal();
					Money deductions = Receipt.INSTANCE.getDeductionTotal();

					if (billItemsOnly.isGreaterThan(tax)
							&& billItemsOnly.isGreaterThan(deductions)) {
						Receipt.INSTANCE.setBaseBill(billItemsOnly);
						Receipt.INSTANCE.calculateTip();
						updateLabels();
					} else {
						myToast.setText(R.string.bad_itemsOnlyBillTotal);
						myToast.show();
						s.clear();
					}
				} catch (NumberFormatException nfe) {
					Log.d("Error", "Error parsing entry");
				}
			}
		});

		/**
		 * Listen for changes in bill deductions. Validate!
		 */
		EditText billDeductions = (EditText) findViewById(R.id.txt_billDeductions);
		billDeductions.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				try {
					Money deductions = new Money(s.toString());
					Money currentTotal = Receipt.INSTANCE.getBaseBill();

					if (deductions.isLessThan(currentTotal)) {
						Receipt.INSTANCE.setDeductionTotal(deductions);
						Receipt.INSTANCE.calculateTip();
						updateLabels();
					} else {
						myToast.setText(R.string.bad_deductions);
						myToast.show();
						s.clear();
					}
				} catch (NumberFormatException nfe) {
					Log.d("Error", "Error parsing entry");
				}
			}
		});

		/**
		 * Listen for changes to tax total. Validate!
		 */
		EditText tax = (EditText) findViewById(R.id.txt_taxTotal);
		tax.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				try {
					Money tax = new Money(s.toString());
					Money currentTotal = Receipt.INSTANCE.getBaseBill();

					if (tax.isLessThan(currentTotal)) {
						Receipt.INSTANCE.setTaxTotal(tax);
						Receipt.INSTANCE.calculateTip();
						updateLabels();
					} else {
						myToast.setText(R.string.bad_deductions);
						myToast.show();
						s.clear();
					}
				} catch (NumberFormatException e) {
					Log.d("Error", "Text failed to parse");
				}
			}
		});
			
	}
	
	
	/**
	 * When the "tailored" label is clicked, take the user to the tip tailoring screen
	 * @param v
	 */
	public void onClick(View v) {
		if(Receipt.INSTANCE.isTipTailored())
			startActivity(new Intent(this, TipTailorActivity.class));
	}

	/**
	 * Load the values 1 -> n where n is the user configured max guests
	 */
	private void populateSpinner() {
		// populate the spinner with integer # of guests
		Spinner numGuests = (Spinner) findViewById(R.id.spinner_NumGuests);
		int maxGuests = MAX_GUESTS;

		List<String> possibleNums = new ArrayList<String>();
		for (int i = 1; i <= maxGuests; i++) {
			possibleNums.add(i + "");
		}
		ArrayAdapter<String> numGuestAdp = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, possibleNums);
		numGuestAdp
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		numGuests.setAdapter(numGuestAdp);
	}

	/**
	 * Use Android SDK to determine the user's location as best guess.
	 * 
	 * @param context
	 * @return the string containing the country ISO 3166 code
	 */
	private Currency getCurrencyByLocation(Context context) {
		TelephonyManager teleMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		String country = null;
		Currency currency = null;
		if (teleMgr != null) {
			country = teleMgr.getSimCountryIso();

			if (country == null) {
				// TODO use fine location
			}

			Locale locale = new Locale("en", country);
			currency = Currency.getInstance(locale);
			String symbol = currency.getSymbol();
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("prefCurrency", symbol);
			editor.commit();

		}

		if (currency == null) {
			currency = Currency.getInstance("USD");
		}

		return currency;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
			Intent i = new Intent(this, SettingsActivity.class);
			startActivityForResult(i, 1);
			break;
		case R.id.tip_tailor:
			Intent j = new Intent(this, TipTailorActivity.class);
			startActivityForResult(j, 1);
			break;
		}
		return true;
	}

	public static void updateLabels() {

		tipRate.setText(new DecimalFormat("#.##").format(Receipt.INSTANCE
				.getTipRate()));
		totalTip.setText(Receipt.INSTANCE.getTipTotal().toString());
		
		if(Receipt.INSTANCE.isTipTailored()) {
			perPersonTip.setText(R.string.lbl_is_tailored);
		} else {
			perPersonTip.setText(Receipt.INSTANCE.getPerPersonTip().toString());
		}
		total.setText(Receipt.INSTANCE.getTotalBill().toString());
		updateTableRowWidgets(activity, layout, TextView.class, "lbl_currency", Receipt.INSTANCE.getSymbol());

	}	

}
