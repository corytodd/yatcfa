package us.corytodd.tipcalculator.data;

import us.corytodd.utils.Money;

public final class Guest {

	// Track the number of guests, each will get a unique id
	private static int count = 1;
	private int id;
	private String fullName;
	private Money amountDue;
	private int   shareOfBill;		// Int converted to percent for calculations

			
	public Guest(String name) {		
		if(name.equalsIgnoreCase("Guest_")) {
			id = count++;
			fullName = name + id;
		} else {
			fullName = name;
		}
	}
	
	
	/**
	 * @return the amountDue
	 */
	public Money getAmountDue() {
		return amountDue;
	}

	/**
	 * @param amountDue the amountDue to set
	 */
	public void setAmountDue(Money amountDue) {
		this.amountDue = amountDue;
	}
	
	
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String name) {
		assert(name != null);
		fullName = name;
	}

	public int getId() {
		return id;
	}
	
	public String toString() {
		return this.getFullName();
	}

	public int getShareOfBill() {
		return shareOfBill;
	}

	public void setShareOfBill(int shareOfBill) {
		this.shareOfBill = shareOfBill;
	}
	
	public static void decrementGuestCount() {
		count--;
	}
	
}
