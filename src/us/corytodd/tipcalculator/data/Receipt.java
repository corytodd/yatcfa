package us.corytodd.tipcalculator.data;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.HashMap;

import us.corytodd.tipcalculator.R;
import us.corytodd.utils.Money;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public enum Receipt {
	INSTANCE;
	
	private Context context;
	private SharedPreferences preferences;
	
	private Money baseBill;
	private Money taxTotal;
	private Money deductionTotal;
	private Money tipableBill;
	private Money tipTotal;
	private Money totalBill;
	
	private Money adjustment;
	
	private int   maxQuality;
	private double starValue;
	
	private double tipRate;
	private double maxTip;
	private double minTip;

	private Money   perPersonTip;
	private boolean isTipTailored;
	private boolean isTipOnTax;
	private boolean isTipOnDeduction;

	private int numGuests;
	private int tippingGuests;
	
	private String symbol;
	
	public static HashMap<Integer, Guest> guestList;

	
	@SuppressLint("UseSparseArrays")
	public void initialize(Context c, int qualityScale) throws ParseException {
		context = c;
		preferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		setSymbol(preferences.getString("prefCurrency", "$"));
		
		// Do not use SparseArray. Guest does not implement proper methods
		guestList = new HashMap<Integer, Guest>();

		baseBill = new Money(BigDecimal.ZERO);
		
		tipRate = Double.parseDouble(preferences.getString("prefDefaultTip",
				context.getString(R.string.basic_twenty)));
		minTip = Double.parseDouble(preferences.getString("prefTipMin", 
				context.getString(R.string.basic_zero)));
		maxTip = Double.parseDouble(preferences.getString("prefTipMax", 
				context.getString(R.string.basic_forty)));
		
		taxTotal = new Money(BigDecimal.ZERO);
		deductionTotal = new Money(BigDecimal.ZERO);
		tipableBill = new Money(BigDecimal.ZERO);
		tipTotal = new Money(BigDecimal.ZERO);
		totalBill = new Money(BigDecimal.ZERO);
		
		adjustment = new Money(BigDecimal.ZERO);

		maxQuality = qualityScale;
		starValue = (tipRate / maxQuality);

		perPersonTip = new Money(BigDecimal.ZERO);

		isTipTailored = false;
		isTipOnTax = preferences.getBoolean("preIncTax", false);
		isTipOnDeduction = preferences.getBoolean("prefIncDeductions", true);
		
		numGuests = 1;
		tippingGuests = 1;			
	}

	public void adjustConfigurables() throws ParseException {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		setSymbol(preferences.getString("prefCurrency", "$"));

		tipRate = Double.parseDouble(preferences.getString("prefDefaultTip",
				context.getString(R.string.basic_twenty)));
		minTip = Double.parseDouble(preferences.getString("prefTipMin", 
				context.getString(R.string.basic_zero)));
		maxTip = Double.parseDouble(preferences.getString("prefTipMax", 
				context.getString(R.string.basic_forty)));

		isTipOnTax = preferences.getBoolean("preIncTax", false);
		isTipOnDeduction = preferences.getBoolean("prefIncDeductions", true);	

		calculateTip();
	}

	public void calculateTip() {
		
		// First get the tipable portion of the bill
		tipableBill = baseBill.add(adjustment);
		
		if(isTipOnTax)
			tipableBill.add(taxTotal);
		
		if(isTipOnDeduction)
			tipableBill.add(deductionTotal);
		
		tipTotal = tipableBill.multiply(getTipAsDecimal());
		perPersonTip = tipTotal.divide(numGuests);
		
		// Alright, now get the total bill including the tip		
		totalBill = baseBill.add(tipTotal).add(taxTotal).subtract(deductionTotal);
		
	}	
	
	public double getTipAsDecimal() {
		return getTipRate() / 100.00;
	}

	public void setRating(int rating) {
		double defTip =  Double.parseDouble(preferences.getString("prefDefaultTip",
				context.getString(R.string.basic_twenty)));
		switch(rating) {
		case 0:
			setTipRate(minTip);
			break;
		case 1:
			setTipRate(defTip * 0.20);
			break;
		case 2:
			setTipRate(defTip * 0.50);
			break;
		case 3:
			setTipRate(defTip);
			break;
		case 4:
			setTipRate(defTip * 1.20);
			break;
		case 5:
			setTipRate(maxTip);
			break;
		}
	}

	public Money getBaseBill() {
		return baseBill;
	}

	public Money getTaxTotal() {
		return taxTotal;
	}

	public Money getDeductionTotal() {
		return deductionTotal;
	}

	public Money getTipableBill() {
		return tipableBill;
	}

	public Money getTotalBill() {
		return totalBill;
	}

	public double getTipRate() {
		return tipRate;
	}

	public double getMaxTip() {
		return maxTip;
	}

	public double getMinTip() {
		return minTip;
	}

	public Money getPerPersonTip() {
		return perPersonTip;
	}

	public boolean isTipTailored() {
		return isTipTailored;
	}

	public boolean isTipOnTax() {
		return isTipOnTax;
	}

	public boolean isTipOnDeduction() {
		return isTipOnDeduction;
	}

	public int getNumGuests() {
		return numGuests;
	}

	public String getSymbol() {
		return symbol;
	}
	
	public Money getTipTotal() {
		return tipTotal;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public void setBaseBill(Money baseBill) {
		this.baseBill = baseBill;
	}

	public void setTaxTotal(Money taxTotal) {
		this.taxTotal = taxTotal;
	}

	public void setDeductionTotal(Money deductionTotal) {
		this.deductionTotal = deductionTotal;
	}

	public void setTipableBill(Money tipableBill) {
		this.tipableBill = tipableBill;
	}

	public void setTotalBill(Money totalBill) {
		this.totalBill = totalBill;
	}

	public void setTipRate(double tipRate) {
		this.tipRate = tipRate;
	}

	public void setMaxTip(double maxTip) {
		this.maxTip = maxTip;
	}

	public void setMinTip(double minTip) {
		this.minTip = minTip;
	}

	public void setPerPersonTip(Money perPersonTip) {
		this.perPersonTip = perPersonTip;
	}

	public void setTipTailored() {
		this.isTipTailored = !isTipTailored;
	}

	public void setTipOnTax(boolean isTipOnTax) {
		this.isTipOnTax = isTipOnTax;
	}

	public void setTipOnDeduction(boolean isTipOnDeduction) {
		this.isTipOnDeduction = isTipOnDeduction;
	}

	public void setNumGuests(int numGuests) {
		this.numGuests = numGuests;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public void setTipTotal(Money tipTotal) {
		this.tipTotal = tipTotal;
	}

	public int getTippingGuests() {
		return tippingGuests;
	}

	public void setTippingGuests(int tippingGuests) {
		this.tippingGuests = tippingGuests;
	}

	public void addGuest(Guest guest, int i) {
		guestList.put(i, guest);
	}
	
	public void removeLastGuest() {
		guestList.remove(guestList.size() - 1);
		Guest.decrementGuestCount();
	}
	
	public int getGuestListSize() {
		return guestList.size();
	}

	public HashMap<Integer, Guest> getGuestList() {
		return guestList;
	}

	public Guest getGuest(int index) {
		return guestList.get(index - 1);
	}
	
	public void makeAdjustment(Money adjustment) {
		this.adjustment = adjustment.add(adjustment);
	}
}
